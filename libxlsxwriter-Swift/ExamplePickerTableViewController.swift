//
//  ExamplePickerTableViewController.swift
//  libxlsxwriter-Swift
//
//  Created by Ludovico Rossi on 12/11/15.
//  Copyright © 2015 Ludovico Rossi. All rights reserved.
//

import UIKit

class ExamplePickerTableViewController: UITableViewController {
    private static let kExampleCellIdentifier = "ExampleCell"
    private static let kViewExampleSegueIdentifier = "ViewExample"
    
    private lazy var swiftExample = SwiftAnatomyExample()
    private lazy var defaultExamples = DefaultExamples.createExamples()
    
    // MARK: - Table View Data Source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // the first section displays the Anatomy Example written in Swift, the second section all the other examples
        return section == 0 ? 1 : defaultExamples!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExamplePickerTableViewController.kExampleCellIdentifier)!
        let example = exampleAtIndexPath(indexPath: indexPath)
        cell.textLabel!.text = example.title
        cell.detailTextLabel!.text = example.subtitle
        
        return cell
    }
    
    // MARK: - View Online
    
    @IBAction func openWebPage() {
        let pageURL = URL(fileURLWithPath: "http://libxlsxwriter.github.io/examples.html")
        UIApplication.shared.openURL(pageURL)
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ExamplePickerTableViewController.kViewExampleSegueIdentifier {
            let vc = segue.destination as! ExampleViewerViewController
            vc.example = exampleAtIndexPath(indexPath: tableView.indexPathForSelectedRow!)
        }
    }
    
    // MARK: Obtain Example
    
    private func exampleAtIndexPath(indexPath: IndexPath) -> Example {
        // the first section displays the Anatomy Example written in Swift, the second section all the other examples
        return indexPath.section == 0 ? swiftExample : defaultExamples![indexPath.row]
    }
}
