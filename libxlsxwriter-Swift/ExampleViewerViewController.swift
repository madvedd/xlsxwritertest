//
//  ExampleViewerViewController.swift
//  libxlsxwriter-Swift
//
//  Created by Ludovico Rossi on 12/11/15.
//  Copyright © 2015 Ludovico Rossi. All rights reserved.
//

import UIKit

class ExampleViewerViewController: UIViewController, UIWebViewDelegate, UIDocumentInteractionControllerDelegate {
    var example: Example!
    
    @IBOutlet private weak var webView: UIWebView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var actionButton: UIBarButtonItem!
    
    private var documentInteractionController: UIDocumentInteractionController!
    
    // The minimum time the loading spinning wheel is shown for (in order to avoid a quick flashing when the loading time is too short)
    private static let kMinLoadingTime = 0.3
    
    // MARK: - View Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // set title
        navigationItem.title = example.title
        
        // start activity indicator
        view.isUserInteractionEnabled = false
        actionButton.isEnabled = false
        activityIndicatorView.startAnimating()
        
        // store current time interval before running the example
        let timeBeforeRun = Date.timeIntervalSinceReferenceDate
        DispatchQueue.global().async() { [weak self] in
            // run example and generate output Excel file
            self?.example.run()
        
            // calculate time elapsed while running the example
            let elapsedTime = Date.timeIntervalSinceReferenceDate - timeBeforeRun
        
            // calculate time we still need to wait (if any) before displaying the output Excel file
            let timeToWaitFor = max(ExampleViewerViewController.kMinLoadingTime - elapsedTime, 0.0)
            DispatchQueue.main.asyncAfter(deadline:DispatchTime.now() + Double(Int64(timeToWaitFor * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { [weak self] in
                if let mySelf = self {
                    // display output Excel file in the web view
                    let fileURL = URL(fileURLWithPath: mySelf.example.outputFilePath)
                    let request = URLRequest(url: fileURL)
                
                    mySelf.webView.loadRequest(request)
                }
            }
        }
    }

    // MARK: - Web View Delegate
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        // stop activity indicator
        activityIndicatorView.stopAnimating()
        view.isUserInteractionEnabled = true
        actionButton.isEnabled = true
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        if navigationType == .linkClicked,
            let url = request.url{
            // open each external link in the default app that handles it
            UIApplication.shared.openURL(url)
            
            return false
        }
        
        return true
    }
    
    // MARK: - Action Button
    
    @IBAction func didTapActionButton(sender: UIBarButtonItem) {
        // show default options for the output Excel file
        let fileURL = URL(fileURLWithPath: example.outputFilePath)
        documentInteractionController = UIDocumentInteractionController(url: fileURL)
        documentInteractionController.delegate = self
        documentInteractionController.presentOptionsMenu(from: sender, animated: true)
    }
}
